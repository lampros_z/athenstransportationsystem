
package storage;

import GUI.GUIController;
import basics.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author it21625,it21622
 */
public class FileDataSource {
    

    private FileWriter writer = null;

    private DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    private int counter = calculateAllLines();
    private int currentLine = 0;
    
    public Map<String, Route> readRoutesByFile(GUIController guiController) {
        guiController.setInfoMsg("Reading routes from files...");
        Map<String, Route> routes = new LinkedHashMap<>();
        BufferedReader file = null;
        try {
            file = new BufferedReader(new InputStreamReader(new FileInputStream("geodata/routes.txt"), StandardCharsets.UTF_8));
                String input;
               
                file.readLine();
                file = new BufferedReader(new InputStreamReader(new FileInputStream("geodata/routes.txt"), StandardCharsets.UTF_8));
                file.readLine();
                while ((input = file.readLine()) != null) {
                    currentLine++;
                    String[] val = input.split(",");
                    Route route = new Route(val[0], val[1], val[2].replaceAll("\"", "").trim(), val[3], Integer.parseInt(val[4]), val[5], val[6]);
                    //Some values have double quotes which we don't want when we are inserting them in the database
                    //We are also using the trim() function to remove the whitespaces from the start and end of the value
                    routes.put(route.getRoute_id(), route);
                    guiController.setProgressBar((int)((currentLine/(counter*1.0))*90000));
                }
                System.out.printf("#####");
            return routes;
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException:  " + e.getMessage());
        } catch (IOException e1) {
            System.out.println("IOException:  " + e1.getMessage());
        } finally {
            try {
                if (file != null) {
                    file.close();
                }
            } catch (IOException e1) {
                System.out.println("IOException:  " + e1.getMessage());
            }
        }
        return routes;
    }

    public boolean readTripsByFile(GUIController guiController) {
        guiController.setInfoMsg("Reading trips from files...");
        BufferedReader file = null;
        try {
            file = new BufferedReader(new InputStreamReader(new FileInputStream("geodata/trips.txt"), StandardCharsets.UTF_8));
            String input;
            file.readLine();
            while ((input = file.readLine()) != null) {
                currentLine++;
                String[] values = input.split(",", -1);
                if (values[5].equals(""))
                    values[5] = "0";

                Trip trip = new Trip(values[0], values[1], values[2], values[3].replaceAll("\"", "").trim(), Integer.parseInt(values[4]), Integer.parseInt(values[5]), values[6]);
                Route route = Route.getRouteByRouteId(trip.getRoute_id());
                route.AddTrip(trip);
                guiController.setProgressBar((int)((currentLine/(counter*1.0))*90000));
            }
            System.out.printf("#####");
            return true;
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException:  " + e.getMessage());
        } catch (IOException e1) {
            System.out.println("IOException:  " + e1.getMessage());
        } finally {
            try {
                if (file != null) {
                    file.close();
                }
            } catch (IOException e1) {
                System.out.println("IOException:  " + e1.getMessage());
            }
        }
        return false;
    }

    public Map<String, Stop> readStopsByFile(GUIController guiController) {
        guiController.setInfoMsg("Reading stops from files...");  
        Map<String, Stop> stops = null;
        BufferedReader file = null;
        try {
            file = new BufferedReader(new InputStreamReader(new FileInputStream("geodata/stops.txt"), StandardCharsets.UTF_8));
            stops = new HashMap<>();

            String input;
            file.readLine();
            while ((input = file.readLine()) != null) {
                currentLine++;
                String[] val = input.split(",");
                Stop stop = new Stop(val[0], val[1], val[2], val[3], Double.parseDouble(val[4]), Double.parseDouble(val[5]), Integer.parseInt(val[6]));
                stops.put(stop.getStop_id(), stop);
                guiController.setProgressBar((int)((currentLine/(counter*1.0))*90000));
            }
            System.out.printf("#####");
            return stops;
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException:  " + e.getMessage());
        } catch (IOException e1) {
            System.out.println("IOException:  " + e1.getMessage());
        } finally {
            try {
                if (file != null) {
                    file.close();
                }
            } catch (IOException e1) {
                System.out.println("IOException:  " + e1.getMessage());
            }
        }
        return stops;
    }


    public List<StopTimes> getStopTimesByFile(GUIController guiController) {
        guiController.setInfoMsg("Reading stop times from files...");
        List<StopTimes> stopTimes = null;
        BufferedReader file = null;
        try {
            file = new BufferedReader(new InputStreamReader(new FileInputStream("geodata/stop_times.txt"), StandardCharsets.UTF_8));
            stopTimes = new ArrayList<>();


            String input;
            file.readLine();

            while ((input = file.readLine()) != null) {
                currentLine++;
                String[] values = input.split(",");

                StopTimes stopTime = new StopTimes(values[0], values[1], Integer.parseInt(values[2]), Integer.parseInt(values[3]), Integer.parseInt(values[4]));
                stopTimes.add(stopTime);
                guiController.setProgressBar((int)((currentLine/(counter*1.0))*90000));
            }
            System.out.printf("#####");
            return stopTimes;
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex2) {
            System.out.println(ex2.getMessage());
        } finally {
            try {
                if (file != null)
                    file.close();
            } catch (IOException ex2) {
                System.out.println(ex2.getMessage());
            }
        }
        return stopTimes;
    }

    public void addLog(String info) {
        try {
            writer.write(info + " on: " + df.format(new Date()));
        } catch (IOException ex) {
            Logger.getLogger(FileDataSource.class.getName()).log(Level.SEVERE, null, ex);
        }
  
    }

    public boolean openStream() {
            try {
                writer = new FileWriter("log.txt",true);
                return true;
            } catch (IOException ex) {
                Logger.getLogger(FileDataSource.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
    }
    
    public void closeStream() 
    {
        try {
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(FileDataSource.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void readDataFromFiles(GUIController guiController) {
        guiController.setInfoMsg("Loading data from files...");
        System.out.println("Loading data from files:");
        System.out.printf("[");
        Map<String, Route> routes = readRoutesByFile(guiController);
        Route.setRoutes(routes);
        readTripsByFile(guiController);
        Map<String, Stop> stops = readStopsByFile(guiController);
        Stop.setStops(stops);
        List<StopTimes> stopTimesByFile = getStopTimesByFile(guiController);
        StopTimes.setStopTimes(stopTimesByFile);
        System.out.printf("]\n");
        //Bind Data
    }

    public int calculateAllLines()
    {
        int counter = 0;
         BufferedReader file = null;
        try {
            file = new BufferedReader(new InputStreamReader(new FileInputStream("geodata/routes.txt"), StandardCharsets.UTF_8));
            counter=getLinesForFile(counter, file);
            file = new BufferedReader(new InputStreamReader(new FileInputStream("geodata/trips.txt"), StandardCharsets.UTF_8));
            counter=getLinesForFile(counter, file);
            file = new BufferedReader(new InputStreamReader(new FileInputStream("geodata/stops.txt"), StandardCharsets.UTF_8));
            counter=getLinesForFile(counter, file);
            file = new BufferedReader(new InputStreamReader(new FileInputStream("geodata/stop_times.txt"), StandardCharsets.UTF_8));
            counter=getLinesForFile(counter, file);
            
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException:  " + e.getMessage());
        } catch (IOException e1) {
            System.out.println("IOException:  " + e1.getMessage());
        } finally {
            try {
                if (file != null) {
                    file.close();
                }
            } catch (IOException e1) {
                System.out.println("IOException:  " + e1.getMessage());
            }
        }
        return counter;
    }
    
    private int getLinesForFile(int counter, BufferedReader file) throws IOException{
        file.readLine();

        while (file.readLine() != null) {
            counter++;
        }
        file.close();
        return counter;
    }
}
