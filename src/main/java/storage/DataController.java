
package storage;

import GUI.GUIController;
import basics.Route;
import basics.Stop;
import basics.StopTimes;
import basics.Trip;
import java.util.Map;

/**
 *
 * @author it21625,it21622
 */
public class DataController implements Runnable{
    FileDataSource fileDataSource;
    GUIController guiController;
    
    public void setGuiController(GUIController guiController){
        this.guiController = guiController;
    }
    
    public void loadData() {
        fileDataSource = new FileDataSource();
        
        
        if(fileDataSource.openStream()){
            DBDataSource db = new DBDataSource();
            boolean openConnection = db.openConnection();
            if (openConnection) {
                int databaseEmpty = db.isDatabaseEmpty();
                if (databaseEmpty > 0) {    //If the database is empty we are reading from the files
                    db.createTables();
                    fileDataSource.readDataFromFiles(guiController);
                    bindData(false);
                    System.out.println("Calculating trip repeats");
                    guiController.setInfoMsg("Calculating trip repeats...");
                    Route.calculateAllTripRepeats(guiController);
                    System.out.println("\nCalculated!");
                    System.out.println("Ready\n");   
                    
                    fileDataSource.addLog("Data has been loaded from files");
                    db.insertValuesFromFiles(guiController);
                    fileDataSource.addLog("Data has been inserted to database");
                } else if (databaseEmpty == 0) {    //Else we are reading from the database
                    db.readDataFromDatabase(guiController);
                    db.closeConnection();
                    bindData(true);
                } else {
                    System.out.println("There was an error");
                }
            }
        } else {
            
            System.out.println("Error : logging system failed to start");
        }
        
    }
    
    private void bindData(boolean isFromDatabase){
        System.out.print("Binding data:");
        guiController.setInfoMsg("Binding data...");
        Trip tripTmp = null;
        boolean flag = false;
        Map<String, Stop> stops = Stop.getStops();
        int stopTimesSize = StopTimes.getStopTimes().size();
        int currentLine=0;
        for (StopTimes stop : StopTimes.getStopTimes()) {
            currentLine++;
            guiController.setProgressBar((int)(90000+(currentLine/(stopTimesSize*1.0))*(isFromDatabase?10000:5000)));
            if (flag && (tripTmp.getTrip_id().equals(stop.getTrip_id()))) {
                Stop stop1 = stops.get(stop.getStop_id());
                tripTmp.addStop(stop1);
                continue;
            }
            for (Route route : Route.getRoutes().values()) {
                if (route.findTrip(stop.getTrip_id()) != null) {
                    flag = true;
                    tripTmp = route.findTrip(stop.getTrip_id());
                    Stop stop1 = stops.get(stop.getStop_id());
                    tripTmp.addStop(stop1);
                }
            }
        }
        System.out.print(" data have successfuly binded\n");
    }

    @Override
    public void run() {
//        DBDataSource db = new DBDataSource();
//        db.openConnection();
//        db.clearData();
//        db.closeConnection();
        loadData();
    }
}
