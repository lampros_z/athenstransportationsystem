package storage;


import GUI.GUIController;
import basics.Route;
import basics.Stop;
import basics.StopTimes;
import basics.Trip;

import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author it21625, it21622
 */

public class DBDataSource implements LocalStrings {
    
    
    private Connection connection;
    private PreparedStatement insertRoutes = null;
    private PreparedStatement insertTrips = null;
    private PreparedStatement insertStops = null;
    private PreparedStatement insertStopTimes = null;
    private PreparedStatement queryCommonStops = null;
    private PreparedStatement insertApiDataToStops = null;
    private PreparedStatement queryStopsByName = null;
    private PreparedStatement updateRouteValues = null;
    private PreparedStatement updateTripRouteIdValue = null;
    private FileDataSource fileDataSource = new FileDataSource();
    private int counter;
    private int currentLine;
    
    public boolean openConnection() {
        try {
            
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DBDataSource.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            connection = DriverManager.getConnection(DB_CONNECTION_URL, DB_USERNAME, DB_PASSWORD);
            if (connection != null) {
                insertRoutes = connection.prepareStatement(INSERT_INTO_ROUTES);
                insertTrips = connection.prepareStatement(INSERT_INTO_TRIPS);
                insertStops = connection.prepareStatement(INSERT_INTO_STOPS);
                insertStopTimes = connection.prepareStatement(INSERT_INTO_STOPS_TIMES);
                queryCommonStops = connection.prepareStatement(QUERY_COMMON_STOPS_BY_STOP_NAME);
                insertApiDataToStops = connection.prepareStatement(INSERT_API_DATA_TO_STOPS_TABLE);
                queryStopsByName = connection.prepareStatement(QUERY_STOPS_BY_NAME);
                updateRouteValues = connection.prepareStatement(UPDATE_ROUTE_WITH_VALUES);
                updateTripRouteIdValue = connection.prepareStatement(UPDATE_TRIP_ROUTE_ID);
                fileDataSource.openStream();
            }
            
            return true;
        } catch (SQLException e) {
            System.out.println("Could not connect to the database:  " + e.getMessage());
            return false;
        }
    }
    
    public boolean closeConnection() {
        try {
            
            //Closing all the resources
            if (insertRoutes != null)
                insertRoutes.close();
            
            if (insertTrips != null)
                insertTrips.close();
            
            if (insertStops != null)
                insertStops.close();
            
            if (insertStopTimes != null)
                insertStopTimes.close();
            
            if (queryCommonStops != null)
                queryCommonStops.close();
            
            if (insertApiDataToStops != null)
                insertApiDataToStops.close();
            
            if (queryStopsByName != null)
                queryStopsByName.close();
            
            if (updateRouteValues != null)
                updateRouteValues.close();
            
            if (updateTripRouteIdValue != null)
                updateTripRouteIdValue.close();
            
            if( fileDataSource != null)
                fileDataSource.closeStream();
            
            if (connection != null)
                connection.close();
            
            return true;
        } catch (SQLException e) {
            System.out.println("Couldn't close connection:  " + e.getMessage());
            return false;
        }
    }
    
    
    public void createTables() {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.execute(CREATE_TABLE_ROUTES);
            statement.execute(CREATE_TABLE_TRIPS);
            statement.execute(CREATE_TABLE_STOPS);
            statement.execute(CREATE_TABLE_STOP_TIMES);
        } catch (SQLException e) {
            System.out.println("CREATION FAILED:  " + e.getMessage());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                System.out.println("Closing statement failed:  " + e.getMessage());
            }
        }
        
    }
    
    public void insertValuesFromFiles(GUIController guiController) {
        try {
            connection.setAutoCommit(false);
            guiController.setInfoMsg("Inserting routes to database...");
            insertRoutesFromFile();
            guiController.setInfoMsg("Inserting trips to database...");
            insertTripsFromFile();
            guiController.setProgressBar(96000);
            guiController.setInfoMsg("Inserting stops to database...");
            insertStopsFromFile();
            guiController.setProgressBar(97500);
            guiController.setInfoMsg("Inserting stop times to database...");
            insertStopTimesFromFile();
            guiController.setProgressBar(100000);
            connection.commit();
        } catch (SQLException e) {
            System.out.println("Insertion failed:  " + e.getMessage());
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                System.out.println("Setting auto commit to default behavior failed" + e.getMessage());
            }
        }
    }
    
    private void insertRoutesFromFile() throws SQLException {
        System.out.print("Inserting routes to the data base" +
                "\n[");
        int i = 0;
        for (Route route : Route.getRoutes().values()) {
            insertRoutes.setString(1, route.getRoute_id());
            insertRoutes.setString(2, route.getRoute_short_name());
            insertRoutes.setString(3, route.getRoute_long_name());
            insertRoutes.setString(4, route.getRoute_desc());
            insertRoutes.setInt(5, route.getRoute_type());
            insertRoutes.setString(6, route.getRoute_color());
            insertRoutes.setString(7, route.getRoute_text_color());
            insertRoutes.addBatch();
            if (i % 500 == 0) {
                insertRoutes.executeBatch();
                insertRoutes.clearBatch();
            }
            
            i++;
        }
        insertRoutes.executeBatch();
        insertRoutes.clearBatch();
        
        System.out.println("INSERTED]");
        fileDataSource.addLog("Routes have been inserted to database from files");
    }
    
    private void insertTripsFromFile() throws SQLException {
        System.out.print("Inserting trips to the data base" +
                "\n[");
        int i = 0;
        for (Route route : Route.getRoutes().values()) {
            for (Trip trip : route.getAllTrips().values()) {
                insertTrips.setString(1, trip.getRoute_id());
                insertTrips.setString(2, trip.getService_id());
                insertTrips.setString(3, trip.getTrip_id());
                insertTrips.setString(4, trip.getTrip_headsign());
                insertTrips.setInt(5, trip.getDirection_id());
                insertTrips.setInt(6, trip.getBlock_id());
                insertTrips.setString(7, trip.getShape_id());
                insertTrips.setInt(8, trip.getTrip_repeats());
                insertTrips.addBatch();
                if (i % 500 == 0) {
                    insertTrips.executeBatch();
                    insertTrips.clearBatch();
                }
                i++;
            }
        }
        insertTrips.executeBatch();
        insertTrips.clearBatch();
        
        System.out.println("INSERTED]");
        fileDataSource.addLog("Trips have been inserted to database from files");
    }
    
    private void insertStopsFromFile() throws SQLException {
        System.out.print("Inserting stops to the data base" +
                "\n[");
        int i = 0;
        for (Stop stop : Stop.getStops().values()) {
            insertStops.setString(1, stop.getStop_id());
            insertStops.setString(2, stop.getStop_code());
            insertStops.setString(3, stop.getStop_name());
            insertStops.setString(4, stop.getStop_desc());
            insertStops.setDouble(5, stop.getStop_lat());
            insertStops.setDouble(6, stop.getStop_lon());
            insertStops.setInt(7, stop.getLocation_type());
            insertStops.setString(8, "");
            insertStops.setString(9, "");
            insertStops.setString(10, "");
            insertStops.addBatch();
            if (i % 500 == 0) {
                insertStops.executeBatch();
                insertStops.clearBatch();
            }
            i++;
        }
        insertStops.executeBatch();
        insertStops.clearBatch();
        System.out.println("INSERTED]");
        fileDataSource.addLog("Stops have been inserted to database from files");
        
    }
    
    private void insertStopTimesFromFile() throws SQLException {
        System.out.print("Inserting stop times to the data base" +
                "\n[");
        int i = 0;
        for (StopTimes stopTime : StopTimes.getStopTimes()) {
            insertStopTimes.setString(1, stopTime.getTrip_id());
            insertStopTimes.setString(2, stopTime.getStop_id());
            insertStopTimes.setInt(3, stopTime.getStop_sequence());
            insertStopTimes.setInt(4, stopTime.getPickup_type());
            insertStopTimes.setInt(5, stopTime.getDrop_off_type());
            insertStopTimes.addBatch();
            if (i % 500 == 0) {
                insertStopTimes.executeBatch();
                insertStopTimes.clearBatch();
            }
            i++;
        }
        insertStopTimes.executeBatch();
        insertStopTimes.clearBatch();
        System.out.println("INSERTED]");
        fileDataSource.addLog("StopTimes have been inserted to database from files");
    }
    
    public List<String> getCommonStopRoutes(String stopName) {
        ResultSet resultSet = null;
        List<String> stopCommonRoute = null;
        try {
            queryCommonStops.setString(1, stopName);
            resultSet = queryCommonStops.executeQuery();
            stopCommonRoute = new ArrayList<>();
            
            while (resultSet.next()) {
                stopCommonRoute.add(resultSet.getString(1));
            }
            return stopCommonRoute;
        } catch (SQLException e) {
            System.out.println("Query failed : " + e.getMessage());
            return stopCommonRoute;
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
            } catch (SQLException e) {
                System.out.println("God damn here :" + e.getMessage());
            }
        }
    }
    
    public Map<String, Route> getAllRoutesFromDB(GUIController guiController) {
        Statement statement = null;
        Map<String, Route> routes = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(QUERY_ROUTES_TABLE);
            routes = new LinkedHashMap<>();
            if(guiController!=null)
                guiController.setInfoMsg("Reading routes from database...");
            System.out.print("Reading routes from db ->");
            while (resultSet.next()) {
                if(guiController!=null)
                    currentLine++;
                routes.put(resultSet.getString(1), new Route(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3),
                        resultSet.getString(4), resultSet.getInt(5), resultSet.getString(6), resultSet.getString(7)));
                if(guiController!=null)
                    guiController.setProgressBar((int)((currentLine/(counter*1.0))*90000));
            }
            System.out.print("  done\n");
//            fileDataSource.addLog("Routes have been loaded from database");
            return routes;
        } catch (Exception e) {
            System.out.println("Query failed : " + e.getMessage());
            return routes;
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                System.out.println("Fatal error: " + e.getMessage());
            }
        }
        
    }
    
    public Map<String, Trip> getAllTripsFromDB(GUIController guiController) {
        Statement statement = null;
        ResultSet resultSet = null;
        Map<String, Trip> trips = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(QUERY_TRIPS_TABLE);
            trips = new HashMap<>();
            if(guiController!=null)
                guiController.setInfoMsg("Reading trips from database...");
            System.out.print("Reading trips from db ->");
            while (resultSet.next()) {
                if(guiController!=null)
                    currentLine++;
                trips.put(resultSet.getString(3), new Trip(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4)
                        , resultSet.getInt(5), resultSet.getInt(6), resultSet.getString(7), resultSet.getInt(8)));
                if(guiController!=null)
                    guiController.setProgressBar((int)((currentLine/(counter*1.0))*90000));
            }
            System.out.print(" done\n");
//            fileDataSource.addLog("Trips have been loaded from database");
            return trips;
        } catch (Exception e) {
            System.out.println("Query failed: " + e.getMessage());
            return trips;
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                System.out.println("fatal error : " + e.getMessage());
            }
        }
    }
    
    public Map<String, Stop> getAllStopsFromDB(GUIController guiController) {
        Statement statement = null;
        Map<String, Stop> stops = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(QUERY_STOPS_TABLE);
            stops = new LinkedHashMap<>();
            guiController.setInfoMsg("Reading stops from database...");
            System.out.print("Reading stops  from db ->");
            while (resultSet.next()) {
                currentLine++;
                stops.put(resultSet.getString(1), new Stop(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3),
                        resultSet.getString(4), resultSet.getDouble(5), resultSet.getDouble(6), resultSet.getInt(7), resultSet.getString(8), resultSet.getString(9), resultSet.getString(10)));
                guiController.setProgressBar((int)((currentLine/(counter*1.0))*90000));
            }
            System.out.print(" done\n");
//            fileDataSource.addLog("Stops have been loaded from database");
            return stops;
        } catch (Exception e) {
            System.out.println("Query failed : " + e.getMessage());
            return stops;
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                System.out.println("Fatal error: " + e.getMessage());
            }
        }
        
    }
    
    public List<StopTimes> getStopTimeFromDB(GUIController guiController) {
        guiController.setInfoMsg("Reading stop times from database...");
        System.out.print("Reading stop times from db ->");
        Statement statement = null;
        List<StopTimes> stopTimes = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(QUERY_STOP_TIMES_TABLE);
            stopTimes = new ArrayList<>();
            while (resultSet.next()) {
                currentLine++;
                stopTimes.add(new StopTimes(resultSet.getString(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4),
                        resultSet.getInt(5)));
                guiController.setProgressBar((int)((currentLine/(counter*1.0))*90000));
            }
            System.out.print(" done\n");
//            fileDataSource.addLog("StopTimes have been loaded from database");
            return stopTimes;
        } catch (Exception e) {
            System.out.println("Query failed : " + e.getMessage());
            return stopTimes;
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                System.out.println("Fatal error: " + e.getMessage());
            }
        }
        
    }
    
    public void clearData() {
        try {
            connection.setAutoCommit(false);
            clearData(DELETE_RECORDS_FROM_STOP_TIMES);
            clearData(DELETE_RECORDS_FROM_STOPS);
            clearData(DELETE_RECORDS_FROM_TRIPS);
            clearData(DELETE_RECORDS_FROM_ROUTES);
            createTables();
            connection.commit();
            
        } catch (SQLException ex) {
            Logger.getLogger(DBDataSource.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.setAutoCommit(true);
                
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        
        
    }
    
    private void clearData(String SQLQuery) {
        Statement statement = null;
        try {
            System.out.println("Deleting data from tables");
            statement = connection.createStatement();
            statement.execute(SQLQuery);
            fileDataSource.addLog("Database has been cleared");
        } catch (SQLException e) {
            System.out.println("Query failed: " + e.getMessage());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                System.out.println("Something went wrong: " + e.getMessage());
            }
        }
    }
    
    public boolean insertIntoRoutes(String route_id, String route_short_name, String route_long_name, String route_desc, int route_type, String route_color, String route_text_color) {
        try {
            insertRoutes.setString(1, route_id);
            insertRoutes.setString(2, route_short_name);
            insertRoutes.setString(3, route_long_name);
            insertRoutes.setString(4, route_desc);
            insertRoutes.setInt(5, route_type);
            insertRoutes.setString(6, route_color);
            insertRoutes.setString(7, route_text_color);
            boolean execute = insertRoutes.execute();
            if (execute)
                fileDataSource.addLog("Route " + route_id + "has been inserted to database");
            return execute;
        } catch (SQLException e) {
            System.out.println("Insertion failed :" + e.getMessage());
            return false;
        }
    }
    
    public boolean insertIntoTrips(String route_id, String service_id, String trip_id, String trip_headsign, int direction_id, int block_id, String shape_id, int trip_repeats) {
        try {
            insertTrips.setString(1, route_id);
            insertTrips.setString(2, service_id);
            insertTrips.setString(3, trip_id);
            insertTrips.setString(4, trip_headsign);
            insertTrips.setInt(5, direction_id);
            insertTrips.setInt(6, block_id);
            insertTrips.setString(7, shape_id);
            insertTrips.setInt(8, trip_repeats);
            fileDataSource.addLog("Trip has been inserted to database");
            boolean execute = insertTrips.execute();
            if (execute)
                fileDataSource.addLog("Trip " + trip_id + "has been inserted to database");
            return execute;
        } catch (SQLException e) {
            System.out.println("Insertion failed :" + e.getMessage());
            return false;
        }
    }
    
    public boolean insertIntoStops(String stop_id, String stop_code, String stop_name, String stop_desc, double stop_lat, double stop_lon, int location_type) {
        try {
            insertStops.setString(1, stop_id);
            insertStops.setString(2, stop_code);
            insertStops.setString(3, stop_desc);
            insertStops.setString(4, stop_name);
            insertStops.setDouble(5, stop_lat);
            insertStops.setDouble(6, stop_lon);
            insertStops.setInt(7, location_type);
            boolean execute = insertStops.execute();
            if (execute)
                fileDataSource.addLog("Stop " + stop_id + "has been inserted to database");
            return execute;
        } catch (SQLException e) {
            System.out.println("Insertion failed :" + e.getMessage());
            return false;
        }
    }
    
    public boolean insertIntoStopTimes(String trip_id, String stop_id, int stop_sequence, int pickup_type, int drop_off_type) {
        try {
            insertStopTimes.setString(1, trip_id);
            insertStopTimes.setString(2, stop_id);
            insertStopTimes.setInt(3, stop_sequence);
            insertStopTimes.setInt(4, pickup_type);
            insertStopTimes.setInt(5, drop_off_type);
            boolean execute = insertStopTimes.execute();
            if (execute)
                fileDataSource.addLog("StopTime " + stop_id + "," + trip_id + " " + "has been inserted to database");
            return execute;
            
        } catch (SQLException e) {
            System.out.println("Insertion failed :" + e.getMessage());
            return false;
        }
    }
    
    public int isDatabaseEmpty() {
        Statement statement = null;
        ResultSet resultSet = null;
        int count = 0;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(CHECK_FOR_TABLES);
            if (!resultSet.next())
                return 1;
            else {
                resultSet = statement.executeQuery(CHECK_FOR_ROUTES);
                resultSet.next();
                count += resultSet.getInt(1);
                resultSet = statement.executeQuery(CHECK_FOR_TRIPS);
                resultSet.next();
                count += resultSet.getInt(1);
                resultSet = statement.executeQuery(CHECK_FOR_STOPS);
                resultSet.next();
                count += resultSet.getInt(1);
                resultSet = statement.executeQuery(CHECK_FOR_STOP_TIMES);
                resultSet.next();
                count += resultSet.getInt(1);
                if (count > 0)
                    return 0;
                else
                    return 1;
            }
        } catch (SQLException e) {
            System.out.println("Query failed :" + e.getMessage());
            return -1;
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                System.out.println("Something went wrong: " + e.getMessage());
                return -1;
            }
        }
    }
    
    public void insertApiDataToStops(String road, String suburb, String county, String stopId) {
        try {
            insertApiDataToStops.setString(1, road);
            insertApiDataToStops.setString(2, suburb);
            insertApiDataToStops.setString(3, county);
            insertApiDataToStops.setString(4, stopId);
            boolean execute = insertApiDataToStops.execute();
            if (execute)
                fileDataSource.addLog("API has been called for stop with stop ID: " + stopId);
        } catch (SQLException e) {
            System.out.println("Query failed : " + e.getMessage());
            fileDataSource.addLog(e.getMessage());
        }
    }
    
    public List<Stop> queryStopsByName(String stopName) {
        try {
            queryStopsByName.setString(1, stopName);
            ResultSet stopsSet = queryStopsByName.executeQuery();
            List<Stop> stops = new ArrayList<>();
            while (stopsSet.next()) {
                stops.add(new Stop(stopsSet.getString(1), stopsSet.getString(2), stopsSet.getString(3), stopsSet.getString(4), stopsSet.getDouble(5), stopsSet.getDouble(6), stopsSet.getInt(7),
                        stopsSet.getString(8), stopsSet.getString(9), stopsSet.getString(10)));
            }
            return stops;
        } catch (SQLException e) {
            System.out.println("Query failed: " + e.getMessage());
            return null;
        }
    }
    
    public String updateRouteValues(String route_id, String route_short_name, String route_long_name, String route_desc, int route_type, String route_color, String route_text_color, String old_id){
        boolean execute = false;
        try {
            connection.setAutoCommit(false);
            
            if(!old_id.equals(route_id)){
                dropFkTripRouteConstraint();
                updateTripRouteId(old_id,route_id);
            }
            updateRouteValues.setString(1, route_id);
            updateRouteValues.setString(2, route_short_name);
            updateRouteValues.setString(3, route_long_name);
            if ( route_desc == null )
                route_desc = "";
            updateRouteValues.setString(4, route_desc);
            updateRouteValues.setInt(5, route_type);
            updateRouteValues.setString(6, route_color);
            updateRouteValues.setString(7, route_text_color);
            updateRouteValues.setString(8, old_id);
            execute = updateRouteValues.execute();
            if(!old_id.equals(route_id)){
                addFkTripRouteConstraint();
            }
            connection.commit();
            return "The database has been updated.";
        } catch (Exception ex) {
            try {
                connection.rollback();
                if(!old_id.equals(route_id)){
                    addFkTripRouteConstraint();
                }
                connection.setAutoCommit(true);
            } catch (SQLException ex1) {
                System.out.println("Fatal error!");
            }
            return ("Error : " + ex.getMessage());
        }
    }
    
    
    private void dropFkTripRouteConstraint() throws SQLException{
        Statement statement=connection.createStatement();
        statement.execute(DROP_FK_TRIPS_ROUTE_CONSTRAINT);
        statement.close();
    }
    
    private void updateTripRouteId(String oldId, String newId) throws SQLException{
        updateTripRouteIdValue.setString(1, newId);
        updateTripRouteIdValue.setString(2, oldId);
        updateTripRouteIdValue.execute();
    }
    
    private void addFkTripRouteConstraint() throws SQLException{
        Statement statement = connection.createStatement();;
        statement.execute(ADD_FK_TRIPS_ROUTE_CONSTRAINT);
        statement.close();
    }
    
    public void countLines()
    {
        counter=0;
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet result = statement.executeQuery(ROUTES_COUNT);
            result.next();
            counter += result.getInt(1);
            result = statement.executeQuery(TRIPS_COUNT);
            result.next();
            counter += result.getInt(1);
            result = statement.executeQuery(STOPS_COUNT);
            result.next();
            counter += result.getInt(1);
            result = statement.executeQuery(STOP_TIMES_COUNT);
            result.next();
            counter += result.getInt(1);
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage());
        } finally {
            try {
                if ( statement != null )
                    statement.close();
            } catch (SQLException ex) {
                System.out.println("fatal error: " + ex.getMessage());
            }
        }
    }
    
    public void readDataFromDatabase(GUIController guiController){
        guiController.setInfoMsg("Loading data from database...");
        System.out.println("Loading data from database");
        countLines();
        Route.setRoutes(getAllRoutesFromDB(guiController));
        Map<String, Trip> allTripsFromDB = getAllTripsFromDB(guiController);
        for (Trip trip : allTripsFromDB.values()) {
            Route route = Route.getRouteByRouteId(trip.getRoute_id());
            route.AddTrip(trip);
        }
        Stop.setStops(getAllStopsFromDB(guiController));
        StopTimes.setStopTimes(getStopTimeFromDB(guiController));
        fileDataSource.addLog("Data has been loaded from database");
    }
}
