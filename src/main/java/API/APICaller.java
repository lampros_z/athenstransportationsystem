
package API;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import basics.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import storage.DBDataSource;


/**
 * @author it21625, it21622
 */

public class APICaller {

    public final String ROAD = "Road";
    public final String SUBURB = "Suburb";
    public final String COUNTY = "County";

    DefaultHttpClient httpClient = new DefaultHttpClient();

    public void callApi(Stop stop) throws IOException {
        if (!stop.hasDataFromApi()) {
            Map<String, String> data = null;

            data = new HashMap<>();
            HttpGet getRequest = new HttpGet("https://nominatim.openstreetmap.org/reverse?format=json&lat=" + stop.getStop_lat() + "&lon=" + stop.getStop_lon() + "&zoom=18&addressdetails=1");
            //Giving lon and lat because some stops have the same name, and we can't tell which is which; coordinates are unique.
            HttpResponse response;
            try {
                response = httpClient.execute(getRequest);
            } catch (java.net.UnknownHostException e) {
                System.out.println("Wrong url.");
                return;
            }
            //is valid?
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != 200) {
                throw new RuntimeException("Failed with HTTP error code : " + statusCode);
            }

            HttpEntity httpEntity = response.getEntity();
            String apiOutput = null;
            try {
                apiOutput = EntityUtils.toString(httpEntity);
            } catch (org.apache.http.ParseException ex) {
                Logger.getLogger(APICaller.class.getName()).log(Level.SEVERE, null, ex);
            }

            JSONParser parser = new JSONParser();

            Object jsonObj = null;
            try {
                jsonObj = parser.parse(apiOutput);
            } catch (ParseException ex) {
                Logger.getLogger(APICaller.class.getName()).log(Level.SEVERE, null, ex);
            }
            JSONObject jsonObject = (JSONObject) jsonObj;

            JSONObject address = (JSONObject) jsonObject.get("address"); //In this specific API it returns JSONObject instead of an JSONArray

            String road = (String) address.get("road");
            data.put("Road", road);

            String suburb = (String) address.get("suburb");
            data.put("Suburb", suburb);

            String county = (String) address.get("county");
            data.put("County", county);

            DBDataSource db = new DBDataSource();
            boolean openConnection = db.openConnection();

            if (openConnection) {
                System.out.println("Connection established");
                db.insertApiDataToStops(data.get(ROAD), data.get(SUBURB), data.get(COUNTY), stop.getStop_id()); //Updating database row
                db.closeConnection();
                shutdown();
            }
            stop.setRoad(data.get(ROAD));
            stop.setSuburb(data.get(SUBURB));
            stop.setCounty(data.get(COUNTY));
        }
    }

    public void shutdown() {
        httpClient.getConnectionManager().shutdown();
    }
}
