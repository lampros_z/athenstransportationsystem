/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package GUI;

import basics.Route;
import basics.Trip;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import storage.DBDataSource;
import javax.swing.table.TableCellRenderer;
/**
 *
 * @author lampros
 */
public class EditRoute extends javax.swing.JFrame {
    
    /**
     * Creates new form EditRoute
     */
    Map<String, Route> routes = Route.getRoutes();
    Route loadedRoute;
        
    public EditRoute() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        initComponents();
        searchField.setText("Type to search...");
        ResetTables();
    }
    
    private void ResetTables(){
        ResetNameTable();
        ResetInfoTable();
        setData();
    }
    
    private void setData(){
        DefaultTableModel model = (DefaultTableModel) jTable2.getModel();
        for(Route route : routes.values())
        {
            if(route.getRoute_id().contains(searchField.getText()))
            {
                model.addRow(new Object[]{
                    route.getRoute_id()
                });
                
            }
        }
    }
    
    private void ResetNameTable(){
        DefaultTableModel model = new DefaultTableModel(){  //We are overriding the isCellEditable method to always return false, so the user cannot edit the cells.
            //We do this by using an Anonymous Inner Class
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        model.addColumn("Route id");
        jTable2.setModel(model);
        jTable2.setAutoCreateRowSorter(true);
        
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<DefaultTableModel>((DefaultTableModel)jTable2.getModel());
        jTable2.setRowSorter(sorter);
        List<RowSorter.SortKey> sortKeys = new ArrayList<>();
        
        int columnIndexToSort = 0;
        sortKeys.add(new RowSorter.SortKey(columnIndexToSort, SortOrder.ASCENDING));
        
        sorter.setSortKeys(sortKeys);
        sorter.sort();
    }
    
    private void ResetInfoTable(){
        DefaultTableModel model = new DefaultTableModel(){  //We are overriding the isCellEditable method to always return false, so the user cannot edit the cells.
            //We do this by using an Anonymous Inner Class
            @Override
            public boolean isCellEditable(int row, int column){
                if(column==5 || column==6)
                    return false;
                else
                    return true;
            }
        };
        model.addColumn("Route id");
        model.addColumn("Route short name");
        model.addColumn("Route long name");
        model.addColumn("Route description");
        model.addColumn("Route type");
        model.addColumn("Route color");
        model.addColumn("Route text color");
        jTable3.setModel(model);
        jTable3.setAutoCreateRowSorter(true);
        
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<DefaultTableModel>((DefaultTableModel)jTable3.getModel());
        jTable3.setRowSorter(sorter);
        List<RowSorter.SortKey> sortKeys = new ArrayList<>();
        
        int columnIndexToSort = 0;
        sortKeys.add(new RowSorter.SortKey(columnIndexToSort, SortOrder.ASCENDING));
        
        sorter.setSortKeys(sortKeys);
        sorter.sort();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        searchField = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable()
        {
            @Override

            public Component prepareRenderer (TableCellRenderer renderer, int rowIndex, int columnIndex){
                Component comp = super.prepareRenderer(renderer, rowIndex, columnIndex);

                Object value = getModel().getValueAt(rowIndex,columnIndex);

                if(columnIndex == 5 || columnIndex == 6){
                    Color c = Color.decode("#"+value);
                    comp.setBackground(c);
                    comp.setForeground(new Color(255-c.getRed(), 255-c.getGreen(), 255-c.getBlue()));
                }

                else {
                    comp.setBackground(Color.WHITE);
                    comp.setForeground(Color.BLACK);
                }

                return comp;
            }

        }
        ;
        submitBtn = new javax.swing.JButton();
        infoMsg = new javax.swing.JLabel();
        getRouteColor = new javax.swing.JButton();
        getRouteTextColor = new javax.swing.JButton();
        colorChooser = new javax.swing.JColorChooser();
        setRouteColor = new javax.swing.JButton();
        setRouteTextColor = new javax.swing.JButton();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTable4MouseReleased(evt);
            }
        });
        jScrollPane4.setViewportView(jTable4);

        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTable5MouseReleased(evt);
            }
        });
        jScrollPane5.setViewportView(jTable5);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        searchField.setToolTipText("");
        searchField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchFieldKeyReleased(evt);
            }
        });

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTable2MouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(jTable3);

        submitBtn.setText("Submit changes");
        submitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitBtnActionPerformed(evt);
            }
        });

        getRouteColor.setText("Get Route color");
        getRouteColor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getRouteColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getRouteColorActionPerformed(evt);
            }
        });

        getRouteTextColor.setText("Get Route text color");
        getRouteTextColor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getRouteTextColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getRouteTextColorActionPerformed(evt);
            }
        });

        setRouteColor.setText("Set Route color");
        setRouteColor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        setRouteColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setRouteColorActionPerformed(evt);
            }
        });

        setRouteTextColor.setText("Set Route text color");
        setRouteTextColor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        setRouteTextColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setRouteTextColorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(searchField)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(infoMsg, javax.swing.GroupLayout.PREFERRED_SIZE, 605, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(colorChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 672, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(submitBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE))
                                    .addComponent(jScrollPane3)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(172, 172, 172)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(setRouteColor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(getRouteColor, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(61, 61, 61)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(getRouteTextColor, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(setRouteTextColor, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(searchField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(infoMsg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(colorChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 386, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(getRouteColor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(getRouteTextColor))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(setRouteColor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(setRouteTextColor)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(submitBtn)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 545, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void searchFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchFieldKeyReleased
        ResetNameTable();
        DefaultTableModel model = (DefaultTableModel) jTable2.getModel();
        for(Route route : routes.values())
        {
            if(route.getRoute_id().contains(searchField.getText().toUpperCase()))
            {
                model.addRow(new Object[]{
                    route.getRoute_id()
                });
            }
        }
    }//GEN-LAST:event_searchFieldKeyReleased

    private void jTable2MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseReleased
        String val =(String) jTable2.getValueAt(jTable2.getSelectedRow(),0);
        ResetInfoTable();
        DefaultTableModel model = (DefaultTableModel) jTable3.getModel();
        Route route = Route.getRouteByRouteId(jTable2.getValueAt(jTable2.getSelectedRow(),0).toString());
        loadedRoute = new Route(route.getRoute_id(), route.getRoute_short_name(), route.getRoute_long_name(), route.getRoute_desc(), route.getRoute_type(), route.getRoute_color(), route.getRoute_text_color());
        model.addRow(new Object[]{
            route.getRoute_id(), route.getRoute_short_name(), route.getRoute_long_name(), route.getRoute_desc(), route.getRoute_type(), route.getRoute_color(), route.getRoute_text_color()
        });
    }//GEN-LAST:event_jTable2MouseReleased

    private void submitBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitBtnActionPerformed
        infoMsg.setText("Updating values... Please wait.");
        final Timer t = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                UpdateValues();
            }
        });
        t.setRepeats(false);
        t.start();
    }//GEN-LAST:event_submitBtnActionPerformed

    public void UpdateValues(){
        DefaultTableModel model = (DefaultTableModel) jTable3.getModel();
        for(int i=0;i<7;i++){
            if(model.getValueAt(0,i)==null){
                model.setValueAt("", 0, i);
            }
        }
        if(model.getValueAt(0,0).toString().equals(loadedRoute.getRoute_id())
              && model.getValueAt(0,1).toString().equals(loadedRoute.getRoute_short_name())
              && model.getValueAt(0,2).toString().equals(loadedRoute.getRoute_long_name())
              && model.getValueAt(0,3).equals(loadedRoute.getRoute_desc())
              && (int)model.getValueAt(0,4) == loadedRoute.getRoute_type()
              && model.getValueAt(0,5).toString().equals(loadedRoute.getRoute_color())
              && model.getValueAt(0,6).toString().equals(loadedRoute.getRoute_text_color())){
          infoMsg.setText("You didn't change any values.");
        }
        else{
            DBDataSource db = new DBDataSource();
            if(db.openConnection())
            {  
                
                String text = db.updateRouteValues(model.getValueAt(0,0).toString().toUpperCase(), model.getValueAt(0,1).toString().toUpperCase(), model.getValueAt(0,2).toString().toUpperCase(), model.getValueAt(0,3).toString().toUpperCase(), (int)model.getValueAt(0,4), model.getValueAt(0,5).toString().toUpperCase(), model.getValueAt(0,6).toString().toUpperCase(), loadedRoute.getRoute_id().toUpperCase());
                infoMsg.setText(text);
                
                Route route = Route.getRouteByRouteId(loadedRoute.getRoute_id().toUpperCase());
                route.updateValues(new Route(model.getValueAt(0,0).toString().toUpperCase(), model.getValueAt(0,1).toString().toUpperCase(), model.getValueAt(0,2).toString().toUpperCase(), model.getValueAt(0,3).toString().toUpperCase(), (int)model.getValueAt(0,4), model.getValueAt(0,5).toString().toUpperCase(), model.getValueAt(0,6).toString().toUpperCase()));
                
                Route.getRoutes().remove(loadedRoute.getRoute_id().toUpperCase());
                Route.getRoutes().put(route.getRoute_id().toUpperCase(), route);
                
                for(Trip trip : route.getAllTrips().values()){
                    trip.setRoute_id(route.getRoute_id());
                }
                
                DefaultTableModel model1 = (DefaultTableModel)jTable2.getModel();
                model1.setRowCount(0);
                model.setRowCount(0);
                
                setData();
                SelectRoute(route.getRoute_id());//In case the user changes the value of the routeId, so we can reload the data after he submits
                db.closeConnection();
            }
        }
    }
    private void getRouteColorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_getRouteColorActionPerformed
        colorChooser.setColor(Color.decode("#"+(jTable3.getRowCount()>0?jTable3.getValueAt(0,5).toString():"FFFFFF")));
    }//GEN-LAST:event_getRouteColorActionPerformed

    private void setRouteColorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setRouteColorActionPerformed
        String hex = String.format("%02x%02x%02x", colorChooser.getColor().getRed(), colorChooser.getColor().getGreen(), colorChooser.getColor().getBlue());  
        jTable3.setValueAt(hex, 0, 5);
    }//GEN-LAST:event_setRouteColorActionPerformed

    private void getRouteTextColorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_getRouteTextColorActionPerformed
        colorChooser.setColor(Color.decode("#"+(jTable3.getRowCount()>0?jTable3.getValueAt(0,6).toString():"FFFFFF")));
    }//GEN-LAST:event_getRouteTextColorActionPerformed

    private void setRouteTextColorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setRouteTextColorActionPerformed
        String hex = String.format("%02x%02x%02x", colorChooser.getColor().getRed(), colorChooser.getColor().getGreen(), colorChooser.getColor().getBlue());  
        jTable3.setValueAt(hex, 0, 6);
    }//GEN-LAST:event_setRouteTextColorActionPerformed

    private void jTable4MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable4MouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jTable4MouseReleased

    private void jTable5MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable5MouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jTable5MouseReleased
    
    private void SelectRoute(String id){
            ResetInfoTable();
            DefaultTableModel model = (DefaultTableModel) jTable3.getModel();
            System.out.println(id);
            Route route = Route.getRouteByRouteId(id);
            loadedRoute = new Route(route.getRoute_id(), route.getRoute_short_name(), route.getRoute_long_name(), route.getRoute_desc(), route.getRoute_type(), route.getRoute_color(), route.getRoute_text_color());
            model.addRow(new Object[]{
                route.getRoute_id(), route.getRoute_short_name(), route.getRoute_long_name(), route.getRoute_desc(), route.getRoute_type(), route.getRoute_color(), route.getRoute_text_color()
            });        
    }

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JColorChooser colorChooser;
    private javax.swing.JButton getRouteColor;
    private javax.swing.JButton getRouteTextColor;
    private javax.swing.JLabel infoMsg;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable4;
    private javax.swing.JTable jTable5;
    private javax.swing.JTextField searchField;
    private javax.swing.JButton setRouteColor;
    private javax.swing.JButton setRouteTextColor;
    private javax.swing.JButton submitBtn;
    // End of variables declaration//GEN-END:variables
}
