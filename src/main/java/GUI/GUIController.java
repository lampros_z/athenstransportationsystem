/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

public class GUIController {
   MainMenu mainMenu;
   
    public void startMenu() {
        mainMenu = new MainMenu();
        mainMenu.setVisible(true);
    }
    
    public void enableButtons(){
        mainMenu.enableButtons();
    }
    
    public MainMenu getMainMenu(){
        return mainMenu;
    }
    
    public void setProgressBar(int value){
        mainMenu.setProgressBarValue(value);
    }
    
    public void setInfoMsg(String msg){
        mainMenu.setInfoMsg(msg);
    }
}
