
package basics;

//import API.APICaller;
import GUI.GUIController;
import storage.DataController;

/**
 * @author it21625, it21622
 */
public class TestBasicClasses {
    
    public static void main(String[] args) throws InterruptedException {
//        APICaller api = new APICaller();
        
        
        GUIController guiController = new GUIController();
        guiController.startMenu();
        
        DataController dataController = new DataController();
        dataController.setGuiController(guiController);
        Thread dataThread = new Thread(dataController,"Data Thread");
        dataThread.setPriority(Thread.MAX_PRIORITY);
        dataThread.start();
        dataThread.join();

        guiController.enableButtons();
  
//            api.callApiForStop("5η ΚΟΡΥΔΑΛΛΟΥ");

    }
    
}
