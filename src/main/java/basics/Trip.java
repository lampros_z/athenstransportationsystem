
package basics;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author it21625, it21622
 */
public class Trip {
    private String route_id;
    private String service_id;
    private String trip_id;
    private String trip_headsign;
    private int direction_id;
    private int block_id;
    private String shape_id;
    private int trip_repeats;

    private Map<String, Stop> stops = new LinkedHashMap<>();

    public Trip(String route_id, String service_id, String trip_id, String trip_headsign, int direction_id, int block_id, String shape_id) {
        this.route_id = route_id;
        this.service_id = service_id;
        this.trip_id = trip_id;
        this.trip_headsign = trip_headsign;
        this.direction_id = direction_id;
        this.block_id = block_id;
        this.shape_id = shape_id;
        this.trip_repeats = 0;
    }

    public Trip(String route_id, String service_id, String trip_id, String trip_headsign, int direction_id, int block_id, String shape_id, int trip_repeats) {
        this.route_id = route_id;
        this.service_id = service_id;
        this.trip_id = trip_id;
        this.trip_headsign = trip_headsign;
        this.direction_id = direction_id;
        this.block_id = block_id;
        this.shape_id = shape_id;
        this.trip_repeats = trip_repeats;
    }

    public void addStop(Stop stop) {
        this.stops.put(stop.getStop_id(), stop);
    }

    public int getBlock_id() {
        return block_id;
    }

    public String getShape_id() {
        return shape_id;
    }

    public String getRoute_id() {
        return route_id;
    }

    public String getService_id() {
        return service_id;
    }

    public String getTrip_id() {
        return trip_id;
    }

    public String getTrip_headsign() {
        return trip_headsign;
    }

    public int getDirection_id() {
        return direction_id;
    }

    public int getTrip_repeats() {
        return trip_repeats;
    }

    public void setTrip_repeats(int trip_repeats) {
        this.trip_repeats = trip_repeats;
    }

    public void setRoute_id(String route_id) {
        this.route_id = route_id;
    }

    public Map<String, Stop> getAllStopsConnectedToThisTrip() {
        return stops;
    }

    public static void getTripInfo(String tripId) {
        Map<String, Route> routeMap = Route.getRoutes();
        for (Route route : routeMap.values()) {
            if (route.findTrip(tripId) != null) {
                System.out.println(route.findTrip(tripId).toString());
                break;
            }
        }
        System.out.println("Trip with tripId:  " + tripId + " have not been found");
    }

    public int getStopsSize() {
        return stops.size();
    }

    public Stop getStopById(String stopId) {
        return stops.get(stopId);
    }

    @Override
    public String toString() {
        return
                "route_id:  " + route_id +
                        "| service_id:  " + service_id +
                        "| trip_id:  " + trip_id +
                        "| trip_headsign:  " + trip_headsign +
                        "| direction_id:  " + direction_id +
                        "| block_id:  " + block_id +
                        "| shape_id:  " + shape_id +
                        "| trip_repeats:  " + trip_repeats;
    }

}
