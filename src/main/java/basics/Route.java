package basics;

//import java.awt.Color;

import GUI.GUIController;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * @author it21625, it21622
 */
public class Route {
    //Basic Fields
    private String route_id;
    private String route_short_name;
    private String route_long_name;
    private String route_desc;
    private int route_type;
    private String route_color;
    private String route_text_color;

    //Maps for the trip types
    private Map<String, Trip> weekdayTrips;
    private Map<String, Trip> weekendTrips;
    private Map<String, Trip> allWeekTrips;

    private static Map<String, Route> routes = null;


    //Color a = Color.decode("#"+route_color);

    private Map<String, Integer> freq = new HashMap<>();

    public Route(String route_id, String route_short_name, String route_long_name, String route_desc, int route_type, String route_color, String route_text_color) {
        this.route_id = route_id;
        this.route_short_name = route_short_name;
        this.route_long_name = route_long_name;
        this.route_desc = route_desc;
        this.route_type = route_type;
        this.route_color = route_color;
        this.route_text_color = route_text_color;
        this.weekdayTrips = new HashMap<>();
        this.weekendTrips = new HashMap<>();
        this.allWeekTrips = new HashMap<>();
    }

    /*Overloaded AddTrip method,that will be called from the main function in order to add a trip in the correct week type*/
    public boolean AddTrip(Trip trip) {
        boolean flag;
        if (trip.getTrip_id().contains("Παρασκευή") || trip.getTrip_id().contains("Δευ-Πεμ") || trip.getTrip_id().contains("Καθημερινή") || trip.getTrip_id().contains("Δευ-Τετ") || trip.getTrip_id().contains("Τρι-Πεμ-Παρ")) {
            flag = AddTrip(0, trip);
        } else if (trip.getTrip_id().contains("Σάββατο") || trip.getTrip_id().contains("Κυριακή") || trip.getTrip_id().contains("Σαβ/Κυρ")) {
            flag = AddTrip(1, trip);
        } else if (trip.getTrip_id().contains("7-ημέρες") || trip.getTrip_id().contains("Δευ-Σαβ")) {
            flag = AddTrip(2, trip);
        } else {
            flag = AddTrip(-1, trip);
        }
        return flag;
    }

    /*This AddTrip method is visible only inside the Route Class.
    Its purpose is to be called from its overloaded method in order to hide functionality from main*/
    private boolean AddTrip(int type, Trip trip) {
        switch (type) {
            case 0:
                this.weekdayTrips.put(trip.getTrip_id(), trip);
                break;
            case 1:
                this.weekendTrips.put(trip.getTrip_id(), trip);
                break;
            case 2:
                this.allWeekTrips.put(trip.getTrip_id(), trip); //Added new type for the 7-ημέρες trip
                break;
            default:
                return false;
        }
        return true;
    }
    
    public void updateValues(Route route){
        this.setRoute_id(route.getRoute_id());
        this.setRoute_desc(route.getRoute_desc());
        this.setRoute_color(route.getRoute_color());
        this.setRoute_long_name(route.getRoute_long_name());
        this.setRoute_short_name(route.getRoute_short_name());
        this.setRoute_text_color(route.getRoute_text_color());
        this.setRoute_type(route.getRoute_type());
    }

    public void setRoute_id(String route_id) {
        this.route_id = route_id;
    }

    public void setRoute_short_name(String route_short_name) {
        this.route_short_name = route_short_name;
    }

    public void setRoute_long_name(String route_long_name) {
        this.route_long_name = route_long_name;
    }

    public void setRoute_desc(String route_desc) {
        this.route_desc = route_desc;
    }

    public void setRoute_type(int route_type) {
        this.route_type = route_type;
    }

    public void setRoute_color(String route_color) {
        this.route_color = route_color;
    }

    public void setRoute_text_color(String route_text_color) {
        this.route_text_color = route_text_color;
    }

    public String getRoute_id() {
        return route_id;
    }

    public String getRoute_short_name() {
        return route_short_name;
    }

    public String getRoute_long_name() {
        return route_long_name;
    }

    public String getRoute_desc() {
        return route_desc;
    }

    public int getRoute_type() {
        return route_type;
    }

    public String getRoute_color() {
        return route_color;
    }

    public String getRoute_text_color() {
        return route_text_color;
    }

    public Map<String, Trip> getWeekdayTrips() {
        return new HashMap<>(this.weekdayTrips);
    }

    public Map<String, Trip> getWeekendTrips() {
        return new HashMap<>(this.weekendTrips);
    }

    public Map<String, Trip> getAllWeekTrips() {
        return new HashMap<>(this.allWeekTrips);
    }

    
    public static Map<String, Route> getRoutes() {
        return routes;
    }

    public static void setRoutes(Map<String, Route> routes) {
        Route.routes = routes;
    }

    public static Route getRouteByRouteId(String routeId) {
        return routes.get(routeId);
    }

    public static void getRouteInfo(String routeId) {
        Route route = routes.get(routeId);
        if (route == null) {
            System.out.println("Route with ID:  " + routeId + " have not been found");
        } else {
            System.out.println(route.toString());
        }
    }

    public Map<String, Trip> getAllTrips() {
        Map<String, Trip> allTrips = new HashMap<>(this.weekdayTrips);
        allTrips.putAll(this.weekendTrips);
        allTrips.putAll(this.allWeekTrips);
        return allTrips;
    }


    @Override
    public String toString() {
        return
                "route_id: " + route_id +
                        "| route_short_name: " + route_short_name +
                        "| route_long_name:  " + route_long_name +
                        "| route_desc:  " + route_desc +
                        "| route_type:  " + route_type +
                        "| route_color:  " + route_color +
                        "| route_text_color:  " + route_text_color +
                        "| freq=" + freq;
    }

    //Function to find the trip of a specific route
    public Trip findTrip(String tripId) {
        if (tripId.contains("Παρασκευή") || tripId.contains("Δευ-Πεμ") || tripId.contains("Καθημερινή") || tripId.contains("Δευ-Τετ") || tripId.contains("Τρι-Πεμ-Παρ")) {
            return this.weekdayTrips.get(tripId);
        } else if (tripId.contains("Σάββατο") || tripId.contains("Κυριακή") || tripId.contains("Σαβ/Κυρ")) {
            return this.weekendTrips.get(tripId);
        } else if (tripId.contains("7-ημέρες") || tripId.contains("Δευ-Σαβ")) {
            return this.allWeekTrips.get(tripId);
        } else {
            return null;
        }
    }

    //Calculating trip repeats, then sending them to the appropriate trips
    public void calculateTripRepeats() {
        Iterator it = weekdayTrips.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            if (freq.containsKey(weekdayTrips.get(key).getService_id())) //For example ΤΗΛΕΜΑ-Τ3-Παρασκευή-01
                freq.put(weekdayTrips.get(key).getService_id(), freq.get(weekdayTrips.get(key).getService_id()) + 1); //Increment its value by one
            else
                freq.put(weekdayTrips.get(key).getService_id(), 1); //Else set it to one
        }
        it = weekendTrips.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            if (freq.containsKey(weekendTrips.get(key).getService_id())) //For example ΤΗΛΕΜΑ-Τ3-Σάββατο-01
                freq.put(weekendTrips.get(key).getService_id(), freq.get(weekendTrips.get(key).getService_id()) + 1); //Increment its value by one
            else
                freq.put(weekendTrips.get(key).getService_id(), 1); //Else set it to one
        }
        it = allWeekTrips.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            if (freq.containsKey(allWeekTrips.get(key).getService_id())) //For example ΤΗΛΕΜΑ-Π2-7-ημέρες-03
                freq.put(allWeekTrips.get(key).getService_id(), freq.get(allWeekTrips.get(key).getService_id()) + 1); //Increment its value by one
            else
                freq.put(allWeekTrips.get(key).getService_id(), 1); //Else set it to one
        }
        //Adding them to the appropriate trip
        it = freq.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>) it.next();

            Iterator it2 = weekdayTrips.keySet().iterator();
            while (it2.hasNext()) {
                String key2 = (String) it2.next();
                if (weekdayTrips.get(key2).getService_id().equals(entry.getKey()))    //Add the correct trip_repeat to the corresponding trip
                    weekdayTrips.get(key2).setTrip_repeats(entry.getValue());
            }
            it2 = weekendTrips.keySet().iterator();
            while (it2.hasNext()) {
                String key2 = (String) it2.next();
                if (weekendTrips.get(key2).getService_id().equals(entry.getKey()))    //Add the correct trip_repeat to the corresponding trip
                    weekendTrips.get(key2).setTrip_repeats(entry.getValue());
            }
            it2 = allWeekTrips.keySet().iterator();
            while (it2.hasNext()) {
                String key2 = (String) it2.next();
                if (allWeekTrips.get(key2).getService_id().equals(entry.getKey()))    //Add the correct trip_repeat to the corresponding trip
                    allWeekTrips.get(key2).setTrip_repeats(entry.getValue());
            }
        }
    }

    public static void calculateAllTripRepeats(GUIController guiController) {
        int routeSize = Route.getRoutes().size();
        int currentLine=0;
        for (Route route : Route.getRoutes().values()) {
            currentLine++;
            guiController.setProgressBar((int)(90000+(currentLine/(routeSize*1.0))*5000));
            route.calculateTripRepeats();
        }
    }


}
