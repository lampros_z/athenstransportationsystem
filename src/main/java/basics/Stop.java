
package basics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author it21625, it21622
 */
public class Stop {
    private String stop_id;
    private String stop_code;
    private String stop_name;
    private String stop_desc;
    private double stop_lat;
    private double stop_lon;
    private int location_type;

    private String road;
    private String suburb;
    private String county;
    //A static map for the stops, that can be called from everywhere, and cannot be modified outside this class.
    private static Map<String, Stop> stops = null;

    public Stop(String stop_id, String stop_code, String stop_name, String stop_desc, double stop_lat, double stop_lon, int location_type) {
        this.stop_id = stop_id;
        this.stop_code = stop_code;
        this.stop_name = stop_name;
        this.stop_desc = stop_desc;
        this.stop_lat = stop_lat;
        this.stop_lon = stop_lon;
        this.location_type = location_type;
        this.road = "Undefined";
        this.suburb = "Undefined";
        this.county = "Undefined";
    }

    public Stop(String stop_id, String stop_code, String stop_name, String stop_desc, double stop_lat, double stop_lon, int location_type, String road, String suburb, String county) {
        this.stop_id = stop_id;
        this.stop_code = stop_code;
        this.stop_name = stop_name;
        this.stop_desc = stop_desc;
        this.stop_lat = stop_lat;
        this.stop_lon = stop_lon;
        this.location_type = location_type;
        setRoad(road);
        setSuburb(suburb);
        setCounty(county);
    }

    public String getStop_id() {
        return stop_id;
    }

    public String getStop_code() {
        return stop_code;
    }

    public String getStop_name() {
        return stop_name;
    }

    public String getStop_desc() {
        return stop_desc;
    }

    public double getStop_lat() {
        return stop_lat;
    }

    public double getStop_lon() {
        return stop_lon;
    }

    public int getLocation_type() {
        return location_type;
    }

    public static Map<String, Stop> getStops() {
        return stops;
    }

    //Method to set the static stops map
    public static void setStops(Map<String, Stop> stopsMap) {
        stops = stopsMap;
    }

    public static Stop getStopByStopId(String stopId) {
        return stops.get(stopId);
    }

    public static void getStopDetails(String stopId) {
        Stop stop = stops.get(stopId);
        if (stop == null) {
            System.out.println("Stop with stop ID:  " + stopId + " have not been found");
        } else {
            System.out.println(stop.toString());
        }
    }

    public static List<Stop> findStopByName(String stopName) {
        List<Stop> stopsByName = new ArrayList<>();
        for (Stop stop : stops.values()) {
            if (stop.getStop_name().equals(stopName)) {
                stopsByName.add(stop);
            }
        }
        return stopsByName;
    }

    public static List<String> findStopIdByName(String stopName) {
        List<String> stopIdList = new ArrayList<>();
        for (Stop stop : stops.values()) {
            if (stop.getStop_name().equals(stopName))
                stopIdList.add(stop.stop_id);
        }
        return stopIdList;
    }

    //Return a set for the common stops
    //We used Set because we want to remove any duplicate values
    public static Set<Route> getRouteCommonStop(String stopId) {
        Set<Route> commonStops = new HashSet<>();
            for (Route route : Route.getRoutes().values()) {
                for (Trip trip : route.getAllTrips().values()) {
                    if (trip.getStopById(stopId) != null) {
                        commonStops.add(route);
                        break;
                    }
                }
            }
        return commonStops;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        if (road == null)
            this.road = "Undefined";
        else
            this.road = road;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        if (suburb == null)
            this.suburb = "Undefined";
        else
            this.suburb = suburb;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        if (county == null)
            this.county = "Undefined";
        else
            this.county = county;
    }

    public boolean hasDataFromApi() {
        if (this.road.equals("Undefined") && this.suburb.equals("Undefined") && this.county.equals("Undefined"))
            return false;
        else
            return true;
    }

    @Override
    public String toString() {
        return "stop_id=" + stop_id + "\nstop_code=" + stop_code + "\nstop_name=" + stop_name + "\nstop_desc=" + stop_desc + "\nstop_lat=" + stop_lat + "\ntop_lon=" + stop_lon + "\nlocation_type=" + location_type + "\nroad=" + road + "\nsuburb=" + suburb + "\ncounty=" + county + "\n\n";
    }


}
