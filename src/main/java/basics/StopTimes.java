
package basics;

import java.util.Collections;
import java.util.List;

/**
 * @author it21625, it21622
 */
public class StopTimes {
    private String trip_id;
    private String stop_id;
    private int stop_sequence; //Order of stop
    private int pickup_type;
    private int drop_off_type;

    private static List<StopTimes> stopTimes = null;

    public StopTimes(String trip_id, String stop_id, int stop_sequence, int pickup_type, int drop_off_type) {
        this.trip_id = trip_id;
        this.stop_id = stop_id;
        this.stop_sequence = stop_sequence;
        this.pickup_type = pickup_type;
        this.drop_off_type = drop_off_type;
    }

    public String getTrip_id() {
        return trip_id;
    }

    public String getStop_id() {
        return stop_id;
    }

    public int getStop_sequence() {
        return stop_sequence;
    }

    public int getPickup_type() {
        return pickup_type;
    }

    public int getDrop_off_type() {
        return drop_off_type;
    }

    public static List<StopTimes> getStopTimes() {
        return stopTimes;
    }

    public static void setStopTimes(List<StopTimes> stopTimes) {
        StopTimes.stopTimes = stopTimes;
    }

    @Override
    public String toString() {
        return "StopTimes{" + "trip_id=" + trip_id + ", stop_id=" + stop_id + ", stop_sequence=" + stop_sequence + ", pickup_type=" + pickup_type + ", drop_off_type=" + drop_off_type + '}';
    }

}
